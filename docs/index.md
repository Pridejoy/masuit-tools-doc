---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "码数吐司库"
  text: "MasuitTools"
  tagline: 全龄段友好的C#.NET万能工具库
  actions:
    - theme: brand
      text: 开始
      link: /guid

features:
  - title: Feature A
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature B
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature C
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---


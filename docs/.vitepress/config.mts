import { defineConfig } from 'vitepress';

// https://vitepress.dev/reference/site-config
export default defineConfig({
    title: '码数吐司库',
    description: '码数吐司库',
    markdown: {
        lineNumbers: true,
    },
    lastUpdated: true,
    themeConfig: {
        logo: '/icon/logo.png',
        // https://vitepress.dev/reference/default-theme-config
        nav: [
            { text: '指♂', link: '/' },
            { text: '参考', link: '/guid' },
            { text: 'Api', link: '/api' },
            { text: '在线体验', link: 'https://replit.com/@ldqk/MasuitToolsDemo?v=1#main.cs' },
            { text: '主站', link: 'https://masuit.org' },
        ],

        sidebar: [
            {
                text: '指南',
                items: [
                    { text: '使用', link: '/guid/' },
                    { text: '使用', link: '/guid' },
                    { text: '功能', link: '/api' },
                ],
            },
        ],
        footer: {
            message:
                '迷恋自留地 | MIT License | <a href="https://beian.miit.gov.cn/" target="_blank">豫ICP备19020414号</a>',
            copyright: `版权所有 © 2019-${new Date().getFullYear()} | DotNET技术分享`,
        },
        socialLinks: [{ icon: 'github', link: 'https://github.com/ldqk/Masuit.Tools' }],
        search: {
            provider: 'local',
        },
        docFooter: {
            prev: '上一页',
            next: '下一页',
        },
        outline: {
            label: '页面导航',
            level: [2, 4],
        },
        lastUpdated: {
            text: '最后更新于',
            formatOptions: {
                dateStyle: 'short',
                timeStyle: 'medium',
            },
        },
        langMenuLabel: '多语言',
        returnToTopLabel: '回到顶部',
        sidebarMenuLabel: '菜单',
        darkModeSwitchLabel: '主题',
        lightModeSwitchTitle: '切换到浅色模式',
        darkModeSwitchTitle: '切换到深色模式',
    },
});
